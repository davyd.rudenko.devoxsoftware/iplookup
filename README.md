## Azure Container Instances Demo project
---
### Purpose of this project

This project was built to gain hands-on experience with some of Azure IaaS services in preparation for the AZ-204 exam, as well as to try building a simple RESTful API using Python. It has only two endpoints - one of them (`GET /ip/{ip}/country`) returns a country in which provided IP resides and the other (`GET /ip/{ip}`) provides more detailed information. 
It relies on a service called [WeatherApi](https://www.weatherapi.com/) to provide basic geolocation capabilities.
You can contact me for details if you want to play with it :-) 

---
### Stack
- **Python 3**
- **Falcon**
Python framework for creating RESTful APIs.
- **MongoDB**
Document-oriented database. Used to cache responses from WeatherApi.
- **Redis**
Key-Value In-Memory database.
- **Docker & Docker Compose**
Containerization platform.
- **Azure Container Registry**
Service for managed private repositories based on Docker Registry.
- **Azure Container Instances**
A "simplest" way to run a Docker container in Azure.
- **Azure Storage Account & Azure File Shares**
Service used to mount Docker volumes to persist MongoDB and provide config for Redis.
---
### Running locally
- Add missing variables to .env file
- Create virtual environment `python3 -m venv .venv`
- Activate environment `source .venv/bin/activate`
- Install requirements `pip install -r src/requirements.txt`
- Run the app `gunicorn -b 0.0.0.0:5000 src.main:app`
---
### Infrastructure Diagram
![service diagram](https://iplookupdemostacc.blob.core.windows.net/diagrams/diagram.drawio%20(2).png "services diagram")