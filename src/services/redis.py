from src.shared_code.providers import get_ip_lookup_redis, get_redis_key_expiration
from typing import Any, Union


class RedisService:
    def __init__(self) -> None:
        self.redis = get_ip_lookup_redis()
        self.expiration_seconds = get_redis_key_expiration()

    def get(self, key: str) -> Union[None, str]:
        value = self.redis.get(key)
        return value.decode('utf-8') if value else None

    def set(self, key: str, value: Any) -> None:
        self.redis.set(key, value, ex=self.expiration_seconds)
