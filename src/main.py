from src.endpoints.ip_country_lookup import IpCountryLookupEndpoint
from src.endpoints.ip_geo_lookup import IpGeoLookupEndpoint
from src.middleware.auth import AuthMiddleware
from dotenv import load_dotenv, find_dotenv
from falcon import App

load_dotenv(find_dotenv())

app = App(middleware=[AuthMiddleware()])

app.add_route("/ip/{ip}/country", IpCountryLookupEndpoint())
app.add_route("/ip/{ip}", IpGeoLookupEndpoint())
