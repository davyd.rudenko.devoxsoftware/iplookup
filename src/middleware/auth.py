from src.shared_code.providers import get_auth_token
from falcon import Request, Response


class AuthMiddleware:
    def __init__(self) -> None:
        self.auth_token = get_auth_token()

    def process_request(self, req: Request, resp: Response):
        token = req.get_header("Authorization")
        if self.auth_token != token:
            resp.status = 401
            resp.media = {"title": "invalid_token"}
            resp.complete = True
