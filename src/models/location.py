from dataclasses_json import dataclass_json
from dataclasses import dataclass


@dataclass_json
@dataclass
class LocationModel:
    id: int
    name: str
    region: str
    country: str
    lat: float
    lon: float
    url: str
