from src.shared_code.providers import get_weatherapi_cache_mongo
from src.entities.weatherapi_data import WeatherApiData
from typing import TypeVar

TData = TypeVar("TData")


class MongoService:
    def __init__(self) -> None:
        self.db = get_weatherapi_cache_mongo()

    def add_data_to_cache(self, data: WeatherApiData[TData]) -> None:
        self.db["cache"].insert_one(data.to_dict())

    def find_in_cache(self, hash: str) -> WeatherApiData:
        document = self.db["cache"].find_one({"hash": hash})
        if document is not None:
            return WeatherApiData.from_dict(document)
        return None
