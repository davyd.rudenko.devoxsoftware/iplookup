from src.entities.weatherapi_data import WeatherApiData
from src.services.weatherapi import WeatherApiService
from src.services.redis import RedisService
from src.services.mongo import MongoService
from falcon import Request, Response
from datetime import datetime
from hashlib import md5


class IpCountryLookupEndpoint:
    def __init__(self) -> None:
        self.redis_service = RedisService()
        self.mongo_service = MongoService()
        self.weatherapi_service = WeatherApiService()

    def on_get(self, req: Request, resp: Response, ip: str) -> None:
        cached_country = self.redis_service.get(ip)

        if cached_country is not None:
            resp.media = {"country": cached_country}
            return

        weatherapi_country = self.weatherapi_service.ip_lookup(ip)

        data = WeatherApiData(
            md5(ip.encode("utf-8")).hexdigest(), datetime.utcnow(), weatherapi_country
        )
        self.mongo_service.add_data_to_cache(data)

        self.redis_service.set(ip, weatherapi_country.country)
        resp.media = {"country": weatherapi_country.country}
