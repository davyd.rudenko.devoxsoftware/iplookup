from src.entities.weatherapi_data import WeatherApiData
from src.services.weatherapi import WeatherApiService
from src.services.mongo import MongoService
from falcon import Request, Response
from datetime import datetime
from hashlib import md5


class IpGeoLookupEndpoint:
    def __init__(self) -> None:
        self.mongo_service = MongoService()
        self.weatherapi_service = WeatherApiService()

    def on_get(self, req: Request, resp: Response, ip: str) -> None:
        ip_hash = md5(ip.encode('utf-8')).hexdigest()
        cached_data = self.mongo_service.find_in_cache(ip_hash)

        if cached_data is not None:
            resp.media = cached_data.data

        weatherapi_data = self.weatherapi_service.ip_lookup(ip)
        self.mongo_service.add_data_to_cache(
            WeatherApiData(ip_hash, datetime.utcnow(), weatherapi_data)
        )
        resp.media = weatherapi_data.to_dict()
