from src.shared_code.constants import DEV_ENVIRONMENT
from pymongo import MongoClient
from redis import Redis
import os


def get_weatherapi_cache_mongo():
    client = MongoClient(os.getenv("MONGODB_CONNECTION_STRING"))
    return client["weatherapi"]


def get_ip_lookup_redis():
    if os.getenv("ENVIRONMENT") == DEV_ENVIRONMENT:
        return Redis(host="localhost", db=1)
    else:
        return Redis(
            host=os.getenv("REDIS_HOST"),
            db=os.getenv("REDIS_DB_ID"),
            username=os.getenv("REDIS_USERNAME"),
            password=os.getenv("REDIS_PASSWORD"),
        )


def get_redis_key_expiration():
    return int(os.getenv("REDIS_KEY_EXPIRATION"))


def get_weatherapi_token():
    return os.getenv("WEATHERAPI_TOKEN")


def get_weatherapi_base_url():
    return os.getenv("WEATHERAPI_BASE_URL")


def get_auth_token():
    return os.getenv("AUTH_TOKEN")
