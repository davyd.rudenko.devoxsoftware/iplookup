from dataclasses_json import dataclass_json
from typing import TypeVar, Generic
from dataclasses import dataclass
import datetime

TData = TypeVar("TData")

@dataclass_json
@dataclass
class WeatherApiData(Generic[TData]):
    hash: str
    timestamp: datetime
    data: TData
