from src.shared_code.providers import get_weatherapi_token, get_weatherapi_base_url
from src.models.location import LocationModel
import requests


class WeatherApiService:
    def __init__(self) -> None:
        self.token = get_weatherapi_token()
        self.base_url = get_weatherapi_base_url()

    def ip_lookup(self, ip: str) -> LocationModel:
        url = f"{self.base_url}search.json?key={self.token}&q={ip}"
        response = requests.get(url)
        locations = LocationModel.schema().loads(response.text, many=True)
        return locations[0]
